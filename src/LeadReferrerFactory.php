<?php

namespace ITPolice\LeadHelpers;

use ITPolice\LeadHelpers\LeadReferrers\Alliance;
use ITPolice\LeadHelpers\LeadReferrers\BankirosHelper;
use ITPolice\LeadHelpers\LeadReferrers\BankiRuHelper;
use ITPolice\LeadHelpers\LeadReferrers\Click2Money;
use ITPolice\LeadHelpers\LeadReferrers\CpaHubHelper;
use ITPolice\LeadHelpers\LeadReferrers\FinServiceHelper;
use ITPolice\LeadHelpers\LeadReferrers\LeadBitHelper;
use ITPolice\LeadHelpers\LeadReferrers\LeadGidHelper;
use ITPolice\LeadHelpers\LeadReferrers\LeadsSuHelper;
use ITPolice\LeadHelpers\LeadReferrers\LeadsTechHelper;
use ITPolice\LeadHelpers\LeadReferrers\LinkprofitHelper;
use ITPolice\LeadHelpers\LeadReferrers\MasterTargetHelper;
use ITPolice\LeadHelpers\LeadReferrers\RubidHelper;
use ITPolice\LeadHelpers\LeadReferrers\SaleAdsHelper;
use ITPolice\LeadHelpers\LeadReferrers\ThreeLidaHelper;
use ITPolice\LeadHelpers\LeadReferrers\UnicomHelper;
use ITPolice\LeadHelpers\LeadReferrers\GuruleadsHelper;
use ITPolice\LeadHelpers\LeadReferrers\YandexHelper;

final class LeadReferrerFactory
{
    /**
     * @param $type
     * @return LeadReferrer
     */
    public static function factory($type)
    {
        if ($type == 'leadbit') {
            return new LeadBitHelper();
        } elseif ($type == '7cd6f3e53eb297' || $type == 'unicom24') {
            return new UnicomHelper();
        } elseif ($type == 'mastertarget') {
            return new MasterTargetHelper();
        } elseif ($type == 'saleads') {
            return new SaleAdsHelper();
        } elseif ($type == 'linkprofit') {
            return new LinkprofitHelper();
        } elseif (mb_strpos($type, 'leads.su') === 0 || mb_strpos($type, 'leadssu') === 0) {
            return new LeadsSuHelper();
        } elseif ($type == 'rubid') {
            return new RubidHelper();
        } elseif ($type == 'Leadgid') {
            return new LeadGidHelper();
        } elseif ($type == '3lida') {
            return new ThreeLidaHelper();
        } elseif ($type == 'cpahub_sale') {
            return new CpaHubHelper();
        }elseif ($type == 'banki.ru' || $type == 'bankiru') {
            return new BankiRuHelper();
        }elseif ($type == 'bankiros') {
            return new BankirosHelper();
        }elseif ($type == 'guruleads') {
            return new GuruleadsHelper();
        } elseif ($type == 'leadstech') {
            return new LeadsTechHelper();
        } elseif ($type == 'yandex' || $type == 'yandex_marketics') {
            return new YandexHelper();
        } elseif ($type == 'click2money') {
            return new Click2Money();
        } elseif ($type == 'alliance') {
            return new Alliance();
        } elseif ($type == 'finuslugi') {
            return new FinServiceHelper();
        }

        return null;
    }
}
