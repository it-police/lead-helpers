<?php

namespace ITPolice\LeadHelpers;


interface LeadReferrer
{
    /**
     * Отправка постбека со статусом "Получено"
     *
     * @param integer $leadId
     * @param array $refData
     * @return mixed
     */
    public function receivePostBack($leadId, $refData);

    /**
     * Отправка постбека со статусом "Выдано"
     *
     * @param integer $leadId
     * @param array $refData
     * @return mixed
     */
    public function approvedPostBack($leadId, $refData);

    /**
     * Отправка постбека со статусом "Отклонено"
     *
     * @param integer $leadId
     * @param array $refData
     * @return mixed
     */
    public function rejectedPostBack($leadId, $refData);

    /**
     * Получаем id оффера
     *
     * @param array $refData
     * @return mixed
     */
    public function getOfferId($refData);

    /**
     * Получаем id вебмастера
     *
     * @param $refData
     * @return mixed
     */
    public function getWmId($refData);
}
