<?php

namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class LeadGidHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'click_hash';
    public $wmIdKey = 'affiliate_id';

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'pending');
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'approved');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        //$this->sendPostBack($leadId, $refData, 'rejected');
    }

    protected function sendPostBack($leadId, $refData, $status) {
        switch ($status) {
            case 'pending':
                $url = "https://go.leadgid.ru/aff_lsr";
                $data = [
                    'offer_id' => env('LEADGID_OFFER_ID'),
                    'adv_sub' => $leadId,
                    'transaction_id' => @$refData['click_hash'],
                ];
                break;
            case 'approved':
                $goalId = env('LEADGID_GOAL_ID', 3794);

                //для нескольких ступеней коллбеков лидогенератору, в зависимости от суммы займа, изменяемый параметр "goal_id"
                //LEADGID_GOAL_ID_AMOUNT_STEPS="{0 : 1, 10001 : 2, 20001 : 3}"

                $amountSteps = json_decode(env('LEADGID_GOAL_ID_AMOUNT_STEPS', '[]'), true) ?? [];
                if(@$refData['amount'] && @$amountSteps && is_array(@$amountSteps) && count(@$amountSteps) > 0){
                    ksort($amountSteps);
                    $maps = array_reverse($amountSteps, true);
                    $amount = (int)$refData['amount'];
                    foreach ($maps as $mapAmount => $mapGoalId){
                        if ($amount > (int)$mapAmount){
                            $goalId = $mapGoalId;
                            break;
                        }
                    }
                }

                $url = "https://go.leadgid.ru/aff_goal";
                $data = [
                    'a' => 'lsr',
                    'goal_id' => $goalId,
                    'adv_sub' => $leadId,
                    'transaction_id' => @$refData['click_hash'],
                ];
                break;
        }

        if(isset($refData['denial_reason'])) {
            //$data['reason'] = $refData['denial_reason_id'];
            //$data['reason_id'] = $refData['denial_reason_id'];
        }

        $url = $url.'?'.http_build_query($data);
        $this->curlQuery($url);
    }

    /**
     * Отправка лида
     * @param $params
     * @return bool|mixed
     */
    public function sendOffer($params)
    {
        $url = 'https://api3.leadgid.ru/api/universal/applications?affiliate_id=' . getenv('LEADGID_AFFILIATE_ID') . '&api_key=' . getenv('LEADGID_API_KEY');
        $response = $this->curlQuery($url, 'POST', $params);
        return $response;
    }

    public function getRegions() {
        //$url = 'https://api3.leadgid.ru/api/universal/regions?affiliate_id=' . getenv('LEADGID_AFFILIATE_ID') . '&api_key=' . getenv('LEADGID_API_KEY');
        //$regions = $this->curlQuery($url);
        $regions = '[{"id":1,"name":"Республика Адыгея","code":"0100000000000"},{"id":2,"name":"Республика Алтай","code":"0400000000000"},{"id":3,"name":"Алтайский край","code":"2200000000000"},{"id":4,"name":"Амурская обл","code":"2800000000000"},{"id":5,"name":"Архангельская обл","code":"2900000000000"},{"id":6,"name":"Астраханская обл","code":"3000000000000"},{"id":7,"name":"г Байконур","code":"9900000000000"},{"id":8,"name":"Республика Башкортостан","code":"0200000000000"},{"id":9,"name":"Белгородская обл","code":"3100000000000"},{"id":10,"name":"Брянская обл","code":"3200000000000"},{"id":11,"name":"Республика Бурятия","code":"0300000000000"},{"id":12,"name":"Владимирская обл","code":"3300000000000"},{"id":13,"name":"Волгоградская обл","code":"3400000000000"},{"id":14,"name":"Вологодская обл","code":"3500000000000"},{"id":15,"name":"Воронежская обл","code":"3600000000000"},{"id":16,"name":"Республика Дагестан","code":"0500000000000"},{"id":17,"name":"Еврейская Аобл","code":"7900000000000"},{"id":18,"name":"Забайкальский край","code":"7500000000000"},{"id":19,"name":"Ивановская обл","code":"3700000000000"},{"id":20,"name":"Республика Ингушетия","code":"0600000000000"},{"id":21,"name":"Иркутская обл","code":"3800000000000"},{"id":22,"name":"Республика Кабардино-Балкарская","code":"0700000000000"},{"id":23,"name":"Калининградская обл","code":"3900000000000"},{"id":24,"name":"Республика Калмыкия","code":"0800000000000"},{"id":25,"name":"Калужская обл","code":"4000000000000"},{"id":26,"name":"Камчатский край","code":"4100000000000"},{"id":27,"name":"Республика Карачаево-Черкесская","code":"0900000000000"},{"id":28,"name":"Республика Карелия","code":"1000000000000"},{"id":29,"name":"Кемеровская обл","code":"4200000000000"},{"id":30,"name":"Кировская обл","code":"4300000000000"},{"id":31,"name":"Республика Коми","code":"1100000000000"},{"id":32,"name":"Костромская обл","code":"4400000000000"},{"id":33,"name":"Краснодарский край","code":"2300000000000"},{"id":34,"name":"Красноярский край","code":"2400000000000"},{"id":35,"name":"Республика Крым","code":"9100000000000"},{"id":36,"name":"Курганская обл","code":"4500000000000"},{"id":37,"name":"Курская обл","code":"4600000000000"},{"id":38,"name":"Ленинградская обл","code":"4700000000000"},{"id":39,"name":"Липецкая обл","code":"4800000000000"},{"id":40,"name":"Магаданская обл","code":"4900000000000"},{"id":41,"name":"Республика Марий Эл","code":"1200000000000"},{"id":42,"name":"Республика Мордовия","code":"1300000000000"},{"id":43,"name":"г Москва","code":"7700000000000"},{"id":44,"name":"Московская обл","code":"5000000000000"},{"id":45,"name":"Мурманская обл","code":"5100000000000"},{"id":46,"name":"Ненецкий АО","code":"8300000000000"},{"id":47,"name":"Нижегородская обл","code":"5200000000000"},{"id":48,"name":"Новгородская обл","code":"5300000000000"},{"id":49,"name":"Новосибирская обл","code":"5400000000000"},{"id":50,"name":"Омская обл","code":"5500000000000"},{"id":51,"name":"Оренбургская обл","code":"5600000000000"},{"id":52,"name":"Орловская обл","code":"5700000000000"},{"id":53,"name":"Пензенская обл","code":"5800000000000"},{"id":54,"name":"Пермский край","code":"5900000000000"},{"id":55,"name":"Приморский край","code":"2500000000000"},{"id":56,"name":"Псковская обл","code":"6000000000000"},{"id":57,"name":"Ростовская обл","code":"6100000000000"},{"id":58,"name":"Рязанская обл","code":"6200000000000"},{"id":59,"name":"Самарская обл","code":"6300000000000"},{"id":60,"name":"г Санкт-Петербург","code":"7800000000000"},{"id":61,"name":"Саратовская обл","code":"6400000000000"},{"id":62,"name":"Республика Саха \/Якутия\/","code":"1400000000000"},{"id":63,"name":"Сахалинская обл","code":"6500000000000"},{"id":64,"name":"Свердловская обл","code":"6600000000000"},{"id":65,"name":"г Севастополь","code":"9200000000000"},{"id":66,"name":"Республика Северная Осетия - Алания","code":"1500000000000"},{"id":67,"name":"Смоленская обл","code":"6700000000000"},{"id":68,"name":"Ставропольский край","code":"2600000000000"},{"id":69,"name":"Тамбовская обл","code":"6800000000000"},{"id":70,"name":"Республика Татарстан","code":"1600000000000"},{"id":71,"name":"Тверская обл","code":"6900000000000"},{"id":72,"name":"Томская обл","code":"7000000000000"},{"id":73,"name":"Тульская обл","code":"7100000000000"},{"id":74,"name":"Республика Тыва","code":"1700000000000"},{"id":75,"name":"Тюменская обл","code":"7200000000000"},{"id":76,"name":"Республика Удмуртская","code":"1800000000000"},{"id":77,"name":"Ульяновская обл","code":"7300000000000"},{"id":78,"name":"Хабаровский край","code":"2700000000000"},{"id":79,"name":"Республика Хакасия","code":"1900000000000"},{"id":80,"name":"Ханты-Мансийский Автономный округ - Югра АО","code":"8600000000000"},{"id":81,"name":"Челябинская обл","code":"7400000000000"},{"id":82,"name":"Республика Чеченская","code":"2000000000000"},{"id":83,"name":"Чувашская Республика","code":"2100000000000"},{"id":84,"name":"Чукотский АО","code":"8700000000000"},{"id":85,"name":"Ямало-Ненецкий АО","code":"8900000000000"},{"id":86,"name":"Ярославская обл","code":"7600000000000"}]';

        return json_decode($regions, JSON_UNESCAPED_UNICODE);
    }

    public function getCities($regionId) {
        $url = 'https://api3.leadgid.ru/api/universal/regions/'.$regionId.'?affiliate_id=' . getenv('LEADGID_AFFILIATE_ID') . '&api_key=' . getenv('LEADGID_API_KEY');
        $cities = [];
        $res = $this->curlQuery($url);

        if(isset($res->cities)) {
            $cities = json_decode(json_encode($res->cities, JSON_UNESCAPED_UNICODE), true);
        }

        return $cities;
    }

    function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $ch = curl_init($URL);
        $headers = array_replace([],$headers);
        if ($method == 'POST') {
            $data_string = urldecode(http_build_query($postData));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $headers[] = 'Content-Length: ' . strlen($data_string);
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        $this->error = [];

        $e = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        Log::debug(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? http_build_query($postData) : false,
            'response' => $result
        ]);

        if ($http_code != 200) {
            $this->error['http_code'] = $http_code;
            $this->error['result'] = $result;
            /*if ($http_code == 400) {
                $this->error['msg'] = "Входные данные содержат ошибку";
            } elseif ($http_code == 401) {
                $this->error['msg'] = "Необходима авторизация";
            } elseif ($http_code == 402) {
                $this->error['msg'] = "Недостаточно средств на счете";
            } elseif ($http_code == 403) {
                $this->error['msg'] = "Доступ запрещен";
            }*/
        }
        if (!$result) {
            $this->error['curl_error'] = $e;
        } else {
            if ($http_code == 200) {
                return json_decode($result);
            }
        }

        return false;
    }

}

?>
