<?php


namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class SaleAdsHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'click';
    public $wmIdKey = 'wm';
    protected $postBackUrl = 'https://my.saleads.pro/pb';

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 0);
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 1);
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, -1);
    }

    protected function sendPostBack($leadId, $refData, $status) {
        $data = [
            'status' => $status,
        ];

        if(isset($refData['denial_reason'])) {
            $data['reason'] = $refData['denial_reason_id'];
            //$data['reason_id'] = $refData['denial_reason_id'];
        }

        $url = $this->postBackUrl.'/'.@$refData['click'].'/'.env('SALEADS_CONDITION').'/'.$leadId.'?'.http_build_query($data);
        return $this->curlQuery($url);
    }

    protected function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $ch = curl_init(urldecode($URL));
        $headers = array_replace([],$headers);
        if ($method == 'POST') {
            $data_string = urldecode(http_build_query($postData));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $headers[] = 'Content-Length: ' . strlen($data_string);
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        Log::debug(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? http_build_query($postData) : false,
            'response' => $result
        ]);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_code == 200) {
            return json_decode($result);
        }

        return false;
    }

}
