<?php


namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class LeadsSuHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'transaction_id';
    public $wmIdKey = 'affiliate_id';
    protected $postBackUrl = 'http://api.leads.su/advertiser/conversion/createUpdate';

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'pending');
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'approved');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'rejected');
    }

    protected function sendPostBack($leadId, $refData, $status) {
        $data = [
            'token' => env('LEADSSU_TOKEN'),
            'goal_id' => '0',
            'adv_sub' => $leadId,
            'status' => $status,
            'comment' => @urlencode(@$refData['denial_reason'])
        ];
        if($status == 'pending') {
            $data['transaction_id'] = @$refData['transaction_id'];
        }

        if($status == 'approved') {
            $data['goal_id'] = env('LEADSSU_GOAL_APPROVED', 0);
        }

        $url = $this->postBackUrl.'?'.http_build_query($data);
        return $this->curlQuery($url);
    }

    protected function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $ch = curl_init(urldecode($URL));
        $headers = array_replace([],$headers);
        if ($method == 'POST') {
            $data_string = urldecode(http_build_query($postData));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $headers[] = 'Content-Length: ' . strlen($data_string);
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        Log::debug(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? http_build_query($postData) : false,
            'response' => $result
        ]);


        if ($http_code == 200) {
            return json_decode($result);
        }

        return false;
    }

}
