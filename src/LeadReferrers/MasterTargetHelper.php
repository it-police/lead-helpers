<?php

namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class MasterTargetHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'click_id';
    public $wmIdKey = 'webmaster_id';
    const URL = "https://mastertarget.ru";
    //
    public $error;

    public function __construct()
    {

    }

    function curlQuery($URL, $method = 'POST', $postData = null)
    {
        $data_string = json_encode($postData);
        $ch = curl_init($URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        if(env('MASTERTARGET_LOG_REQUESTS')) {
            Log::info('Mastertarget response log', [
                'url' => $URL,
                'method' => $method,
                'params' => $data_string,
                'response' => $result
            ]);
        }

        $this->error = [];

        $e = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($http_code != 200) {
            $this->error['http_code'] = $http_code;
            $this->error['result'] = $result;
            /*if ($http_code == 400) {
                $this->error['msg'] = "Входные данные содержат ошибку";
            } elseif ($http_code == 401) {
                $this->error['msg'] = "Необходима авторизация";
            } elseif ($http_code == 402) {
                $this->error['msg'] = "Недостаточно средств на счете";
            } elseif ($http_code == 403) {
                $this->error['msg'] = "Доступ запрещен";
            }*/
        }
//        var_dump($URL);
//        var_dump($data_string);
        //var_export($result);
        if (!$result) {
            $this->error['curl_error'] = $e;
        } else {
            if ($http_code == 200 || $http_code == 201) {
                return json_decode($result, true);
            }
        }

        return false;
    }

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'pending');
    }

    public function approvedPostBack($leadId, $refData)
    {
        //$this->sendPostBack($leadId, $refData, 'confirmed');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        //$this->sendPostBack($leadId, $refData, 'declined');
    }

    protected function sendPostBack($leadId, $refData, $status) {
        $data = [
            'AccountId' => env('MASTERTARGET_ACCOUNT_ID'),
            'CampaignID' => env('MASTERTARGET_CAMPAIGN_ID'),
            'TotalCost' => 0,
            'OrderID' => $leadId,
            'ProductID' => 'sale',
            'AffiliateID' => @$refData['webmaster_id'],
            'visitorId' => @$refData['click_id'],
        ];

        if(isset($refData['denial_reason'])) {
            //$data['reason'] = $refData['denial_reason_id'];
            //$data['reason_id'] = $refData['denial_reason_id'];
        }

        $url = self::URL . "/scripts/sale.php?".http_build_query($data);
        $res = $this->curlQuery($url);
        Log::info('Mastertarget Send Offer Postback', [
            $url,
            $res
        ]);
    }

}

?>
