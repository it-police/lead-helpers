<?php

namespace ITPolice\LeadHelpers\LeadReferrers;

trait LeadReferrerTrait
{
    public function getOfferId($refData)
    {
        return @$refData[$this->offerIdKey];
    }

    public function getWmId($refData)
    {
        return @$refData[$this->wmIdKey];
    }
}
