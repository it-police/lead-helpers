<?php


namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class FinServiceHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'clickid';
    public $wmIdKey = 'cnv_id';
    protected $postBackUrl = 'http://54081f.binomlink.com/click.php';

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'pending');
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'approved');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'rejected');
    }

    protected function sendPostBack($leadId, $refData, $status) {
        $data = [
            'cnv_status' => $status,
            'clickid' => @$refData['click_id'],
            'cnv_id' => @$refData['wmid'],
        ];

        $url = $this->postBackUrl.'?'.http_build_query($data);
        return $this->curlQuery($url);
    }

    protected function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $ch = curl_init(urldecode($URL));
        $headers = array_replace([],$headers);
        if ($method == 'POST') {
            $data_string = urldecode(http_build_query($postData));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $headers[] = 'Content-Length: ' . strlen($data_string);
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        Log::debug(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? http_build_query($postData) : false,
            'response' => $result
        ]);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_code == 200) {
            return json_decode($result);
        }

        return false;
    }

}
