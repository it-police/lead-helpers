<?php


namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class LinkprofitHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'click_id';
    public $wmIdKey = 'transaction_id';

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'R');
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'A');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'D');
    }

    protected function sendPostBack($leadId, $refData, $status) {

        if($status == 'R') {
            $data = [
                'OrderID' => $leadId,
                'ClickHash' => @$refData['click_id'],
                'CampaignID' => env('LINKPROFIT_CAMPAIGN_ID'),
                'AffiliateID' => @$refData['wm_id'],
            ];
            $postBackUrl = 'https://cpa.linkprofit.ru/sale';
        } else {
            $data = [
                'Status' => $status
            ];
            $postBackUrl = 'http://s.linkprofit.ru/postback/'.env('LINKPROFIT_PARTNER_CODE').'/update/'.$leadId;
        }

        if(isset($refData['denial_reason'])) {
            $data['reason'] = $refData['denial_reason_id'];
            //$data['reason_id'] = $refData['denial_reason_id'];
        }

        $url = $postBackUrl.'?'.http_build_query($data);
        $res = $this->curlQuery($url);
        Log::info('Linkprofit Send Offer Postback', [
            $url,
            $res
        ]);
        return $res;
    }

    protected function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $ch = curl_init(urldecode($URL));
        $headers = array_replace([],$headers);
        if ($method == 'POST') {
            $data_string = urldecode(http_build_query($postData));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            $headers[] = 'Content-Length: ' . strlen($data_string);
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        Log::debug(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? http_build_query($postData) : false,
            'response' => $result
        ]);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_code == 200) {
            return json_decode($result);
        }

        return false;
    }

}
