<?php


namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class YandexHelper implements LeadReferrer
{

    use LeadReferrerTrait;

    public $offerIdKey = 'yclid';
    public $wmIdKey = 'utm_campaign';

    public function receivePostBack($leadId, $refData)
    {
        //$this->sendPostBack($leadId, $refData, 'pending');
    }

    public function approvedPostBack($leadId, $refData)
    {
        //$this->sendPostBack($leadId, $refData, 'approved');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        //$this->sendPostBack($leadId, $refData, 'rejected');
    }

    protected function sendPostBack() {

    }

}
