<?php

namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class CpaHubHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'transaction_id';
    public $wmIdKey = 'wm_id';
    public $error;

    public function __construct()
    {
    }

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'pending');
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'approved');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'rejected');
    }

    protected function sendPostBack($leadId, $refData, $status) {
        $data = [
            'status' => $status,
            'token' => getenv('CPAHUB_TOKEN'),
            'transaction_id' => @$refData['transaction_id'],
            'adv_sub' => $leadId
        ];

        if(isset($refData['denial_reason'])) {
            //$data['reason'] = $refData['denial_reason_id'];
            //$data['reason_id'] = $refData['denial_reason_id'];
        }

        $url = 'http://client-api.cpahub.ru/adv/create-conversion?'.http_build_query($data);
        $res = $this->curlQuery($url);
    }

    function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $ch = curl_init($URL);
        $headers = array_replace([],$headers);
        if ($method == 'POST') {
            $data_string = urldecode(http_build_query($postData));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $e = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $this->error = [];


        Log::debug(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? http_build_query($postData) : false,
            'response' => $result
        ]);

        if ($http_code != 200) {
            $this->error['http_code'] = $http_code;
            $this->error['result'] = $result;
            /*if ($http_code == 400) {
                $this->error['msg'] = "Входные данные содержат ошибку";
            } elseif ($http_code == 401) {
                $this->error['msg'] = "Необходима авторизация";
            } elseif ($http_code == 402) {
                $this->error['msg'] = "Недостаточно средств на счете";
            } elseif ($http_code == 403) {
                $this->error['msg'] = "Доступ запрещен";
            }*/
        }
        if (!$result) {
            $this->error['curl_error'] = $e;
        } else {
            if ($http_code == 200) {
                return json_decode($result);
            }
        }

        return false;
    }

}

?>
