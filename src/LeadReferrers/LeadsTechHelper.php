<?php

namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class LeadsTechHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'clickid';
    public $wmIdKey = 'wmid';

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 0);
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 1);
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 2);
    }

    protected function sendPostBack($leadId, $refData, $status) {
        $url = "https://offers.leads.tech/add-conversion";

        $goalId = 0;
        if($status == 1) {
            $goalId = env('LEADSTECH_GOAL_ID', 3);

            // для нескольких ступеней коллбеков лидогенератору, в зависимости от суммы займа, изменяемый параметр "goal_id"//example env params
            // LEADSTECH_GOAL_ID_AMOUNT_STEPS="{0 : 3, 10001 : 2, 20001 : 3}"

            $amountSteps = json_decode(env('LEADSTECH_GOAL_ID_AMOUNT_STEPS', '[]'), true) ?? [];
            if(@$refData['amount'] && @$amountSteps && is_array(@$amountSteps) && count(@$amountSteps) > 0) {
                ksort($amountSteps);
                $maps = array_reverse($amountSteps, true);
                $amount = (int)$refData['amount'];
                $sumConfirm = null;
                foreach ($maps as $mapAmount => $mapArray){
                    if ($amount > (int)$mapAmount){
                        $goalId = $mapArray['goal_id'];
                        $sumConfirm = $mapArray['sumConfirm'] ?? null;
                        break;
                    }
                }
            }
        }

        $data = [
            'status' => $status,
            'goal_id' => $goalId,
            'transaction_id' => $leadId,
            'click_id' => @$refData['clickid'],
        ];

        if(@$sumConfirm) {
            $data['sumConfirm'] = @$sumConfirm;
        }


        if(isset($refData['denial_reason'])) {
            //$data['reason'] = $refData['denial_reason_id'];
            //$data['reason_id'] = $refData['denial_reason_id'];
        }

        $url = $url.'?'.http_build_query($data);
        $this->curlQuery($url);
    }

    function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $ch = curl_init($URL);
        $headers = array_replace([],$headers);
//        if ($method == 'POST') {
//            $data_string = urldecode(http_build_query($postData));
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
//            $headers[] = 'Content-Length: ' . strlen($data_string);
//            $headers[] = 'Content-Type: application/x-www-form-urlencoded';
//        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);

        $this->error = [];

        $e = curl_error($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        Log::debug(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? http_build_query($postData) : false,
            'response' => $result
        ]);

        if ($http_code != 200) {
            $this->error['http_code'] = $http_code;
            $this->error['result'] = $result;
            /*if ($http_code == 400) {
                $this->error['msg'] = "Входные данные содержат ошибку";
            } elseif ($http_code == 401) {
                $this->error['msg'] = "Необходима авторизация";
            } elseif ($http_code == 402) {
                $this->error['msg'] = "Недостаточно средств на счете";
            } elseif ($http_code == 403) {
                $this->error['msg'] = "Доступ запрещен";
            }*/
        }
        if (!$result) {
            $this->error['curl_error'] = $e;
        } else {
            if ($http_code == 200) {
                return json_decode($result);
            }
        }

        return false;
    }

}

?>
