<?php

namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class UnicomHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'click_id';
    public $wmIdKey = 'wm_id';
    const URL = "https://unicom24.ru";

    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'receive');
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'accept');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'reject');
    }

    protected function sendPostBack($leadId, $refData, $status)
    {
        $clickId = @$refData['click_id'];
        $url = self::URL . "/offer/postback/{$clickId}/?status=$status";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $res = curl_exec($ch);
        curl_close($ch);

        Log::info('Unicom Send Offer Postback', [
            $url,
            $res
        ]);
        return $res;
    }
}

?>
