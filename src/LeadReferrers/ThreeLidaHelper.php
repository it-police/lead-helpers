<?php


namespace ITPolice\LeadHelpers\LeadReferrers;

use ITPolice\LeadHelpers\LeadReferrer;
use Illuminate\Support\Facades\Log;

class ThreeLidaHelper implements LeadReferrer
{
    use LeadReferrerTrait;

    public $offerIdKey = 'click_id';
    public $wmIdKey = 'wm_id';
    public function receivePostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'processing');
    }

    public function approvedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'issued');
    }

    public function rejectedPostBack($leadId, $refData)
    {
        $this->sendPostBack($leadId, $refData, 'rejected');
    }

    protected function sendPostBack($leadId, $refData, $status) {
        $data = [
            'status' => $status,
            'LoanID' => $leadId,
            'click_id' => @$refData['click_id'],
        ];

        if(isset($refData['denial_reason'])) {
            //$data['reason'] = $refData['denial_reason_id'];
            //$data['reason_id'] = $refData['denial_reason_id'];
        }

        $url = 'https://api.3lida.ru/v1/postback/';
        $res = $this->curlQuery($url, 'POST', $data);
        Log::info('3Lida Send Offer Postback', [
            $url,
            $res
        ]);
    }

    protected function curlQuery($URL, $method = 'GET', $postData = null, $headers = [])
    {
        $data_string = json_encode($postData);
        $ch = curl_init($URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if(env('3LIDA_TEST_MODE')) {
            curl_setopt($ch, CURLOPT_USERPWD, getenv('3LIDA_TEST_LOGIN') . ":" . getenv('3LIDA_TEST_PASSWORD'));
        } else {
            curl_setopt($ch, CURLOPT_USERPWD, getenv('3LIDA_LOGIN') . ":" . getenv('3LIDA_PASSWORD'));
        }

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        Log::info(__CLASS__.' response log', [
            'url'      => $URL,
            'method'   => $method,
            'params'   => ($method == 'POST') ? json_encode($postData, JSON_UNESCAPED_UNICODE) : false,
            'response' => $result
        ]);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_code == 200) {
            return json_decode($result);
        }

        return false;
    }

    public function sendOffer($data) {
        if(env('3LIDA_TEST_MODE')) {
            $advUrl = 'https://test.3lida.ru/v1/lead/';
        } else {
            $advUrl = 'https://lk.3lida.ru/v1/lead/';
        }
        return $this->curlQuery($advUrl, 'POST', $data);
    }

}
